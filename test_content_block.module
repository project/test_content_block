<?php


/**
 * Implementation of hook_menu()
 */
function test_content_block_menu() {
	$items['test-content-block/generate/%'] = array(
		'title' => '',
		'page callback' => 'test_content_block_generate_node',
		'page arguments' => array(2),
		'access callback' => TRUE,
		'type' => MENU_CALLBACK,
	);
	
	$items['admin/settings/test-content-block'] = array(
		'title' => 'Test Content Block',
		'description' => t('Choose specific nodes for the Test Content block'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('test_content_block_admin_settings'),
		'access arguments' => array('administer test content block'),
		'type' => MENU_NORMAL_ITEM,
	);
	
	$items['test-content-block/autocomplete'] = array(
		'title' => 'Autocomplete',
		'access callback' => TRUE,
		'page callback' => 'test_content_block_autocomplete',
		'type' => MENU_CALLBACK,
	);
	
	return $items;
}


/**
 * Implementation of hook_perm()
 */
function test_content_block_perm() {
	return array('administer test content block');
}


/**
 * Implementation of hook_block()
 */
function test_content_block_block($op = 'list', $delta = 0, $edit = array()) {
	
	if ($op == 'list') {
		$blocks = array();
		$blocks['test_content']['info'] = t('Test Content Block');
		
		return $blocks;
	}
	
	else if ($op == 'view') {
		$block = array();
		switch ($delta) {
			
			case 'test_content':
				$block['subject'] = t('Test Content');
				$block['content'] = test_content_block_contents();
				return $block;
			
			default:
				break;
		}
	}
}

/**
 * Renders contents of the test content block
 */
function test_content_block_contents() {
	$output = '';
	$types = node_get_types('types');
	
	foreach($types as $type) {
		$output .= '<li><strong>' . $type->name . '</strong>: ';
		
		// See if user has manually set which node should show up for this type
		$nid = variable_get("tcb_{$type->type}_nid", '');
		if($nid != '') {
			preg_match('/\[nid: (\d+)\]$/', $nid, $matches);
	    	$nid = $matches[1];
	    	if (!empty($nid)) {
				$q = 'SELECT n.title FROM {node} n WHERE nid=%d AND type=\'%s\'';
				$res = db_query($q, $nid, $type->type);
				$node = db_fetch_object($res);
				
				if(!empty($node)) {
					$output .= '<a href="/node/'.$nid.'">'.$node->title.'</a> (<a href="/node/'.$nid.'/edit">edit</a>)';
					continue;
				}
			}
		}
		
		// If configuration is incorrect and node cannot be found, try auto selecting
		$q = 'SELECT n.nid, n.title FROM {node} n WHERE type=\'%s\' ORDER BY created DESC LIMIT 1';
		$res = db_query($q, $type->type);
		$node = db_fetch_object($res);
	
		if(!empty($node)) {
			$output .= '<a href="/node/'.$node->nid.'">'.$node->title.'</a> (<a href="/node/'.$node->nid.'/edit">edit</a>)';
		}
		// If auto selecting a node fails, show links to create a node of this type
		else {
			$output .= '<p style="padding:0 0 0 10px; margin:0 0 0 10px;">';
			if(function_exists('devel_generate_menu')) {
				$output .= '(<a href="/test-content-block/generate/'.$type->type.'?destination='.$_GET['q'].'">generate</a>)<br>';
			}	
			$output .= '(<a href="/node/add/'.$type->type.'">add manually</a>)</p>';
		}
		$output .= '</li>';
	}
	$output .= '</ul>';
	
	return $output;
}


/**
 * Wrapper function for devel_generate_content_add_node, invoked by 'generate' link on block
 */
function test_content_block_generate_node($type) {
	global $user;
	$results = array(
		'node_types' => array($type => $type),
		'users' => array($user->uid),
		'time_range' => 1000,
	);
	
	require_once(drupal_get_path('module','devel') . '/devel_generate.inc');
	devel_generate_content_add_node($results);
	$q = drupal_get_destination();
	if ($q) {
		drupal_goto($q);
	}
	else {
		drupal_goto('/');
	}
}


function test_content_block_admin_settings() {
	
	$form = array();
	$form['header'] = array(
		'#type' => 'markup',
		'#value' => '<h2>Featured content per content type</h2>',
	);
	
	foreach(node_get_types('types') as $type) {
		$form["tcb_{$type->type}_nid"] = array(
		'#title' => "{$type->name}",
	    '#type' => 'textfield',
	    '#autocomplete_path' => 'test-content-block/autocomplete/'. $type->type,
	    '#default_value' => variable_get("tcb_{$type->type}_nid", ''),
	  );
	}
	
	return system_settings_form($form);
}


/**
 * Page callback for autocomplete.
 */
function test_content_block_autocomplete($type, $string = NULL) {
	$matches = _test_content_block_autocomplete($type, $string);
	drupal_json(drupal_map_assoc($matches));
}
	
function _test_content_block_autocomplete($type, $string) {
	$output = array();

	if (!$type || !$string) {
	  return $output;
	}

	$nodes = test_content_block_api_autocomplete($type, $string);
	return $nodes;
}


/**
 * Fetch a list of nodes available to a given subqueue
 * for autocomplete.
 *
 * @param $queue
 *   The queue that owns the subqueue
 * @param $subqueue
 *   The subqueue
 * @param $string
 *   The string being matched.
 *
 * @return
 *   An keyed array $nid => $title
 */
function test_content_block_api_autocomplete($type, $string) {
  $matches = array();
  if (empty($string)) {
    return $matches;
  }
  $where_args = array();
  global $user;
  if (!user_access('administer nodes', $user)) {
    $where = '(n.status = 1 || n.uid = %d) AND ';
    $where_args[]= $user->uid;
  }
  
  $where .= 'n.type = '. db_placeholders($type, 'varchar');
  $where_args[] = $type;

  // Run a match to see if they're specifying by nid.
  $preg_matches = array();
  $match = preg_match('/\[nid: (\d+)\]/', $string, $preg_matches);
  if (!$match) {
    $match = preg_match('/^nid: (\d+)/', $string, $preg_matches);
  }

  if ($match) {
    // If it found a nid via specification, reduce our resultset to just that nid.
    $where .= " AND n.nid = %d";
    $where_args[] = $preg_matches[1];
  }
  else {
    // Build the constant parts of the query.
    $where .= " AND LOWER(n.title) LIKE LOWER('%s%%')";
    $where_args[] = $string;
  }

//	return array($where, serialize($where_args));

  $result = db_query_range(db_rewrite_sql("SELECT n.nid, n.title FROM {node} n WHERE $where"), $where_args, 0, 10);
  while ($node = db_fetch_object($result)) {
    $matches[$node->nid] = check_plain($node->title) ." [nid: $node->nid]";
  }

  return $matches;
}


