
Test Content Block for Drupal 6.x


Installation
------------
Test Content Block can be installed like any other Drupal module -- place it in
the 'sites/all/modules' (or a site or profile specific module directory) directory
for your site and enable it on the 'admin/build/modules' page.

Then, enable the block wherever you want to be able to see it using the Block
module interface (admin/build/block) or Context (admin/build/context).


Features
--------
The block will give you a list of all content types on your site.  Under each
content type will be either:
	1) a link to a node of that type 
	
OR, if there are no nodes of that type,

	2) a link to manually add a node of that type AND a link to auto-generate
	   the node (if devel_generate is installed).

You can manually set which nodes show up in the block using the controls on the
configuration page (admin/settings/test-content-block).  If you do not, the most
recent node of a given type will be used.



Maintainers
-----------
brynbellomy (Bryn Bellomy)